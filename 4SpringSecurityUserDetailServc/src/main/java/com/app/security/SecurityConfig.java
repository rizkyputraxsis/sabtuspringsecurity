package com.app.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.app.services.UserDetailService;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {
	
	@Bean
	public UserDetailsService uds() {
		return new UserDetailService();
	}
	
	@Bean
	public BCryptPasswordEncoder passencode() {
		return new BCryptPasswordEncoder();
	}
	
	//autentikasi
	public void configAuthentication(AuthenticationManagerBuilder amb)throws Exception  {
		amb.userDetailsService(uds()).passwordEncoder(passencode());
	}
	
	//otorisasi
	public void configure(HttpSecurity hs) throws Exception{
		hs.csrf().disable()
		.authorizeRequests()
		.antMatchers("/listUser").hasRole("ADMIN")
		.antMatchers("/listUserById/**").hasRole("STAF")
		//.anyRequest().anonymous()
		.anyRequest().authenticated()
		.and()
		.httpBasic()
		.and()
		.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);//sesion disable
	}

}
