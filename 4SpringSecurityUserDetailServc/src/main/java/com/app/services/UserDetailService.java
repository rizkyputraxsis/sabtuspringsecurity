package com.app.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.User.UserBuilder;
import org.springframework.stereotype.Service;

import com.app.models.UserModel;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;


public class UserDetailService implements UserDetailsService {
	
	@Autowired
	private UserServices us;
	

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		System.out.println(username);
		UserModel um = us.findByUserName(username);
		UserBuilder builder = null;
		if (um != null) {
			//user ada
			builder = User.withUsername(username);//org.springframework.security.core.userdetails.User
			builder.password(new BCryptPasswordEncoder().encode(um.getPassword()));
			builder.roles(um.getRole());
		} else {
			//user tidak ada
			throw new UsernameNotFoundException("User name tersebut tidak ada!!!");
		}
		
		return builder.build();
	}

}
