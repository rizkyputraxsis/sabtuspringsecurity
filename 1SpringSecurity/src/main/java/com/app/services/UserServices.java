package com.app.services;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.models.UserModel;
import com.app.repository.UserRepository;

@Service
@Transactional
public class UserServices {
	
	@Autowired
	private UserRepository ur;
	
	public List<UserModel> listUser() {
		return ur.findAll();
	}
	
	public Optional<UserModel> listUserById(int id) {
		return ur.findById(id);
	}
	
	public void saveUser(UserModel um) {
		ur.save(um);
	}
	
	public void delUser(int id) {
		ur.deleteById(id);
	}

}
