package com.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.app.models.UserModel;

public interface UserRepository extends JpaRepository<UserModel, Integer> {
	
	//native

}
