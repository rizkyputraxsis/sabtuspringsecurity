package com.app.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.app.models.UserModel;
import com.app.services.UserServices;

@RestController
public class UserControlRest {
	
	@Autowired
	private UserServices us;
	
	@GetMapping("listUser")
	public List<UserModel> listUser(){
		return us.listUser();
	}
	
	@GetMapping("listUserById/{id}")
	public Optional<UserModel>listUserById(@PathVariable int id){
		return us.listUserById(id);
	}
	
	@PostMapping("addUser")
	public void addUser(@RequestBody UserModel um) {
		us.saveUser(um);
	}
	
	@PutMapping("editUser")
	public void editUser(@RequestBody UserModel um) {
		us.saveUser(um);
	}
	
	@DeleteMapping("delUser/{id}")
	public void delUser(@PathVariable int id) {
		us.delUser(id);
	}

}
