package com.app.security;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {
	
	@Autowired
	private DataSource ds;
	
	//autentikasi
	@Autowired
	public void configAuthentication(AuthenticationManagerBuilder amb)throws Exception  {
		amb.jdbcAuthentication()
		.dataSource(ds)
		.usersByUsernameQuery("select user_name,concat('{noop}',password),enabled from m_user where user_name=?")
		.authoritiesByUsernameQuery("select user_name,role from m_user where user_name=?");
	}
	
	//otorisasi
	public void configure(HttpSecurity hs) throws Exception{
		hs.csrf().disable()
		.authorizeRequests()
		.antMatchers("/listUser").hasRole("ADMIN")//kolom role harus di dahului 'ROLE_'
		.antMatchers("/listUserById").hasRole("STAF")
		//.anyRequest().anonymous()
		.anyRequest().authenticated()
		.and()
		.httpBasic()
		.and()
		.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);//sesion disable
	}

}
